export interface InstanceStats {
  totalInstances: number
  totalUsers: number
  totalVideos: number
  totalVideoComments: number
  totalVideoViews: number
}
